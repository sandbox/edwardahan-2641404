<?php

/**
 * @file
 * Hook documentation for the WxPay Mobile module.
 */


/**
 * Allows modules to alter the data array used to create a WxPay Mobile request.
 *
 * @param &$data
 *   The data array used to create mobile request.
 * @param $order
 *   The full order object the request is being generated for.
 *
 * @see commerce_wxpay_mobile_order_data()
 */
function hook_commerce_wxpay_mobile_order_data_alter(&$data, $order) {
  // No example.
}
